package com.example.demo.repository;

import com.example.demo.model.User;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class UserRepository {

    private final Map<Integer, User> usersDatabase;

    private final int MAX_INCORRECT_LOGINS = 3;

    public UserRepository() {
        usersDatabase = new HashMap<>();

        usersDatabase.put(1, new User("cracker", "cracker1234", true, 0));
        usersDatabase.put(2, new User("marry", "marietta!#09", true, 0));
        usersDatabase.put(3, new User("silver", "$silver$", true, 0));
    }

    public boolean checkLogin(final String login, final String password) {
        // TODO: Prosze dokonczyc implementacje...

        int key = getKey(login);

        if(key != 0){

            if(usersDatabase.get(key).isActive()){

                if(usersDatabase.get(key).getPassword().equals(password)){

                    // zerowanie licznika
                    usersDatabase.get(key).setIncorrectLoginCounter(0);

                    return true;
                }else{

                    usersDatabase.get(key).setIncorrectLoginCounter(usersDatabase.get(key).getIncorrectLoginCounter() + 1);
                    if(usersDatabase.get(key).getIncorrectLoginCounter() >= MAX_INCORRECT_LOGINS){
                        usersDatabase.get(key).setActive(false);
                    }

                }

            }

        }

        return false;
    }

    public boolean isActive(String login){
        int key = getKey(login);
        return usersDatabase.get(key).isActive();
    }

    public Integer getKey(String login){
        AtomicInteger key = new AtomicInteger();

        usersDatabase.forEach(
                (k,v) -> {
                    if(v.getLogin().equals(login)){
                        key.set(k);
                    }
                }
        );

        return key.get();
    }

}
